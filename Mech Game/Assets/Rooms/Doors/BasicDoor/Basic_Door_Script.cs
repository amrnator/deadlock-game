﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basic_Door_Script : MonoBehaviour , DoorInterface {

	private GameObject parent;

	void Start(){
		parent = transform.root.gameObject;
	}

	public  void open(int type){
		print ("Openning Door");
		//disable parent's collider
		parent.GetComponent<BoxCollider>().enabled = false;

		//enable the door's collider
		this.GetComponent<BoxCollider> ().enabled = true;

		//part this door
		if(gameObject.name.Equals("Basic_Door_R")){
			//open Right Door
			transform.Translate(new Vector3(2,0,0));
		}
		else{
			//open left Door
			transform.Translate(new Vector3(-2,0,0));
		}
	}

	public  void close(int type){
		print ("Closing Door");
		//enable parent's collider
		parent.GetComponent<BoxCollider>().enabled = true;

		//disable the door's collider
		this.GetComponent<BoxCollider> ().enabled = false;

		//move this door back
		if(gameObject.name.Equals("Basic_Door_R")){
			//close Right Door
			transform.Translate(new Vector3(-2,0,0));
		}
		else{
			//close left Door
			transform.Translate(new Vector3(2,0,0));
		}
	}
}
