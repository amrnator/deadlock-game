﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour {

	public GameObject parentdoor;

	//close door when player leaves trigger area
	void OnTriggerExit(){
		print("Player left");

		parentdoor.BroadcastMessage( "close" , 1);
	}

	//close door when player enters the trigger area
	void OnTriggerEnter(){
		print("Player enters");

		parentdoor.BroadcastMessage( "open" , 1);
	}
}
