﻿using UnityEngine;
using System.Collections;

public class WeaponPickup : InteractionObject{

	GameObject weaponCoord1;
	GameObject weaponCoord2;


	//gun the pickup produces
	public WeaponBehavior weapon;

	void Start(){
		weaponCoord1 = GameObject.Find("WeaponCoord1");
		weaponCoord2 = GameObject.Find("WeaponCoord2");
	}

	//activate: equips the player with weapon
	//instantiates the weapon,  parents it to the correct weapon coord
	// 1 is left (WeaponCoord1), 2 is right(WeaponCoord2)
	public override void activate(int type){
		WeaponBehavior myWeapon = Instantiate (weapon);

		//equip to left hand
		if(type == 1){
			//check if there is a weapon in hand already
			if (weaponCoord1.transform.childCount > 0) {
				//if there's a weapon, destroy it
				Destroy (weaponCoord1.transform.GetChild (0).gameObject);
			}
			//set weapon controlls to left click and make it a child to the coord
			myWeapon.fireButton = "Fire1";
			myWeapon.coord = weaponCoord1;
			myWeapon.transform.position = weaponCoord1.transform.position;
			myWeapon.transform.rotation = weaponCoord1.transform.rotation;
			myWeapon.makeActive ();
		}
		//equip to Right
		if (type == 2) {
			if (weaponCoord2.transform.childCount > 0) {
				//if there's a weapon, destroy it
				Destroy (weaponCoord2.transform.GetChild (0).gameObject);
			}
			//set weapon controlls to left click and make it a child to the coord
			myWeapon.fireButton = "Fire2";
			myWeapon.coord = weaponCoord2;
			myWeapon.transform.position = weaponCoord2.transform.position;
			myWeapon.transform.rotation = weaponCoord2.transform.rotation;
			myWeapon.makeActive ();
		}
	}


}
