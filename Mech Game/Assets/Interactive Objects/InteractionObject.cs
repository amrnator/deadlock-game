﻿using UnityEngine;
using System.Collections;

public abstract class InteractionObject : MonoBehaviour {

	//texture that shows on hud when object is being inspected
	public Texture2D interactTexture;

	//returns the texture
	public Texture2D getMessage (){
		return interactTexture;
	} 

	//activate is called when the user interacts with the object 
	//It enables the object's active functionality, whatever it maybe
	// int type is used to specify actions in objects with multiple functions, like how weapons can be equiped on either hand
	public abstract void activate (int type);
}
