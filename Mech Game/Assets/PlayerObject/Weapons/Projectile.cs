﻿using UnityEngine;
using System.Collections;

public class Projectile : PoolObject {

    public int damage;

    public float magnitude;

    public Vector3 direction;

    public Rigidbody rb;

    void awake() {
        rb = GetComponent<Rigidbody>();
    }

    


}
