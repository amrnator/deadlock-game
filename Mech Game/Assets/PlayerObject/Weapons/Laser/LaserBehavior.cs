﻿using UnityEngine;
using System.Collections;

public class LaserBehavior : WeaponBehavior {

	protected LineRenderer laser;

    protected GameObject player;

	//boolean for delaying damaage instances
	private bool canDamage;

	// Use this for initialization
	void Start () {
		laser = gameObject.GetComponent(typeof(LineRenderer)) as LineRenderer;
        player = GameObject.FindGameObjectWithTag("MainCamera");
		canDamage = true;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButton(fireButton))
		{
			firePressed = 1;
        }
		else {
			firePressed = 0;
			laser.enabled = false;
		}

	}

	void FixedUpdate(){
		if (firePressed == 1) {
			fire ();
		}
	}

	public override void fire(){
		//draw laser
		laser.enabled = true;

        Vector3 target = player.GetComponent<TargetRay>().getTarget();

        Vector3 fwd = target - transform.TransformDirection(Vector3.forward);

        RaycastHit hit;

		if (Physics.Raycast(player.transform.position , player.transform.forward, out hit) )
		{
            if (hit.collider == null)
            {
                print("null");
            }
            else {
				print("Object hit : " + hit.collider.tag);
            }
			if(hit.collider.tag == "Enemy" && canDamage){
            	//deal damage to enemy
				hit.collider.gameObject.GetComponent<Entity>().takeDamage(damage);  
				//start the delay
				canDamage = false;
				StartCoroutine (DamageDelay(.25f));
			}
		}
	}

	IEnumerator DamageDelay(float delay){
		yield return new WaitForSeconds(delay);
		canDamage = true;
	}

}

