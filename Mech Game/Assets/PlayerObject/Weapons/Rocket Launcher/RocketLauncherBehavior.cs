﻿using UnityEngine;
using System.Collections;

public class RocketLauncherBehavior : WeaponBehavior{

	public GameObject projectile;

	public float fireRate = 1.0F;
	private float nextFire = 0.0F;

    //create pool
    void Start() {
        Debug.Log("pool created");
        PoolManager.instance.CreatePool(projectile, 3);
    }

	void Update () {
		if (Input.GetButton(fireButton) && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			fire();
		}


	}

    //get an object from the pool of Rockets
	public override void fire(){
        PoolManager.instance.ReuseObject(projectile, transform.position, transform.rotation * Quaternion.Euler(Vector3.up * 180));
	}
}
