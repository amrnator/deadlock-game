﻿using UnityEngine;
using System.Collections;

public class LazerBehavior : MonoBehaviour {
	GameObject coord;

	LineRenderer laser;

    private int fire;

    // Find the weapon coord object
    void Start () {
		laser = gameObject.GetComponent(typeof(LineRenderer)) as LineRenderer;
        coord = GameObject.FindGameObjectWithTag("WeaponCoord2");
        transform.position = coord.transform.position;
        transform.SetParent(coord.transform);
        fire = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Fire1"))
        {
            fire = 1;
        }
        else {
            fire = 0;
        }


    }

    void FixedUpdate()
    {
        if (fire == 1)
        {
            //draw laser
			StartCoroutine(firelaser());

			Vector3 fwd = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, -fwd, 10))
            {
                Debug.DrawRay(transform.position * 10, -fwd, Color.green, 1000);
                print("There is something in front of the object!");
            }
            else {
                print("NOTHING");
            }
        }
        //print("NOTHING");
    }

	IEnumerator firelaser(){
		laser.enabled = true;
		yield return new WaitForSeconds (0.25f);
		laser.enabled = false;
		//yield return new WaitForSeconds (0.25f);
	}


}
