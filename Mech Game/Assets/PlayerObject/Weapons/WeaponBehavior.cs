﻿using UnityEngine;
using System.Collections;

//class that all weapons inherit from

public abstract class WeaponBehavior : MonoBehaviour {

	//WeaponCoord that this weapon will be parented to
	public GameObject coord;

	//string name of button that shoots
	public string fireButton;

	//int that keeps track of firing
	protected int firePressed = 0;

	//eneables functionality and parents object to coord after instantiation and variables are set
	public void makeActive(){

		transform.SetParent(coord.transform);

		firePressed = 0;
	}

	//fire() shoots the gun
	//called when the fire button is pressed in Update()
	public abstract void fire();

	public int damage;
}
