﻿using UnityEngine;
using System.Collections;

//Basic interface that all weapons implement

public interface BasicWeaponInterface {
	//fire() shoots the gun
	//called when the fire button is pressed in Update()
	void fire();
}
