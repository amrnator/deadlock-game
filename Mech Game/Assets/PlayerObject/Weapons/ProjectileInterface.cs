﻿using UnityEngine;
using System.Collections;

public interface ProjectileInterface  {
    void OnObjectReuse();
}
