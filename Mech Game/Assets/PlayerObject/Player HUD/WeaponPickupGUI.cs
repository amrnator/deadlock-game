﻿using UnityEngine;
using System.Collections;

public class WeaponPickupGUI : MonoBehaviour {

	public Texture2D weaponPickupTex;

	public int messagescale = 1;

	private bool draw = false;

	void toggleWeaponPickupTexture(){

		draw = !draw;

	}

	void OnGUI(){
		if (draw) {
			GUI.DrawTexture (new Rect ((Screen.width - weaponPickupTex.width * messagescale) / 2, (Screen.height - weaponPickupTex.height * messagescale) / 2, weaponPickupTex.width * messagescale, weaponPickupTex.height * messagescale), weaponPickupTex);
		}
	}
}
