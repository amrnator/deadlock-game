﻿using UnityEngine;
using System.Collections;

public class InteractiveGUI : MonoBehaviour {

	public Texture2D interactionMessage;

	public int messagescale = 1;

	private bool draw = false;


	public void setMessageTexture(Texture2D messageTexture){

		interactionMessage = messageTexture;

	}

	public void toggleInteractionMessage(){

		draw = !draw;

	}

	void OnGUI(){
		if (draw) {
			GUI.DrawTexture (new Rect ((Screen.width - interactionMessage.width * messagescale) / 2, (Screen.height - interactionMessage.height * messagescale) / 2, interactionMessage.width * messagescale, interactionMessage.height * messagescale), interactionMessage);
		}
	}
}
