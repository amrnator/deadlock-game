﻿using UnityEngine;
using System.Collections;
//Entity: basic characteristics of an enemy, player, or boss
//mostly heath and damage
public class Entity : MonoBehaviour {
    //health
    public int health;

	// Use this for initialization
    //override this in children
	public void Start () {
        
	}
	
	// Update is called once per frame
	public void Update () {
        if (health <= 0) {
            Die();
        }
	}

    public void Die(){
        //destroy self
		print("Death of Tag: " + this.gameObject.tag);
        GameObject.Destroy(gameObject);
    }

    public void takeDamage(int damage) {
        health = health - damage;
    }

}
