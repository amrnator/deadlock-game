﻿using UnityEngine;
using System.Collections;

public class InteractionRay : MonoBehaviour {

	public GameObject gui;

	InteractiveGUI hud;

	//bool checks if player's crosshair is on an interactible object
	bool lookingAtInteract = false;

	//bool to allow for a delay between interaction calls, so they aren't called multiple times per button press
	bool interactDelay = true;

	//hit object
	RaycastHit hit;

	void Start(){
		hud =  gui.GetComponent<InteractiveGUI> ();
	}

	//constantlyly shoots an array foreward looking for innteractible objects
	void FixedUpdate () {
		Vector3 fwd = transform.TransformDirection (Vector3.forward);

		if (Physics.Raycast (transform.position, fwd, out hit, 5.0f)) {

			//this block is for when the player is looking at an interactive object
			if (hit.collider.gameObject.CompareTag ("interactive") && !lookingAtInteract) {

				InteractionObject hitObj = hit.transform.gameObject.GetComponent<InteractionObject> ();
				//set prompt
				hud.setMessageTexture(hitObj.getMessage());
				//show option on HUD to interact with Object
				hud.toggleInteractionMessage();
				lookingAtInteract = true;
			} else if (!hit.collider.gameObject.CompareTag ("interactive") && lookingAtInteract) {
				hud.toggleInteractionMessage();
				lookingAtInteract = false;
			}


		} else if (lookingAtInteract) {
			hud.toggleInteractionMessage();
			lookingAtInteract = false;
		}
	}

	void Update(){
		//if the player presses the button, activate the object
		//right (Q) sends type 2, left (E) send type 1
		if (Input.GetButton("Right") && lookingAtInteract && interactDelay)
		{
			//hit.transform.gameObject.GetComponent<WeaponPickup>().activate(2);
			hit.transform.gameObject.SendMessage ("activate", 2);
			interactDelay = false;
			StartCoroutine (interactionDelay ());
		}
		if (Input.GetButton("Left") && lookingAtInteract && interactDelay)
		{
			hit.transform.gameObject.SendMessage ("activate", 1);
			interactDelay = false;
			StartCoroutine (interactionDelay ());
		}
	}

	IEnumerator interactionDelay(){
		yield return new WaitForSeconds(.2f);
		interactDelay = true;
	}
}
