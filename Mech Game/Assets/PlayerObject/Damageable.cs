﻿using UnityEngine;
using System.Collections;

public interface Damageable {
    //take damage
    void takeDamage(int damage);
    //die
    void death();
	

}
