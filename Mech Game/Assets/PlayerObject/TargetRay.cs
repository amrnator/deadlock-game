﻿using UnityEngine;
using System.Collections;

public class TargetRay : MonoBehaviour {

    Vector3 target;

    RaycastHit hit;
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, fwd, out hit))
        {
            target = hit.point;
            //print("There is something in front of the object!");
        }
        else
        {
            target = transform.forward;
            //print("NOTHING");
        }
            
   }

    public Vector3 getTarget() {
        return target; 
    }
}
